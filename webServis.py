# -*- coding: utf-8 -*-

from core.reapy import ReaPy
from apps.appdata import AppData

re_api = ReaPy
rest_server = re_api.rest_server()
rest_server.server()
app = rest_server.app

rest_server.api.add_resource(AppData, '/report-server', methods=['GET', 'POST'])

rest_server.api.init_app(app)

config = re_api.configuration().get_configuration()
print('visit : http://'+config['api_web_hostname_ip_address']+':'+config['api_web_hostname_port'])

app.run(config['api_web_hostname_ip_address'], config['api_web_hostname_port'], debug=True)
