


in_file = '/home/makseli/yazilim/proda/ty-11__belgeservis/deneme/wordi.docx'
out_file = '/home/makseli/yazilim/proda/ty-11__belgeservis/deneme/word.pdf'


import sys
import subprocess
import re


def convert_to(folder=None, source=None, timeout=None):

    folder = '/home/makseli/yazilim/proda/ty-11__belgeservis/deneme'
    source = 'wordi.docx'

    args = [libreoffice_exec(), '--headless', '--convert-to', 'pdf', '--outdir', folder, source]

    process = subprocess.run(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, timeout=timeout)
    filename = re.search('-> (.*?) using filter', process.stdout.decode())

    # exit(type(filename))

    if filename is None:
        raise LibreOfficeError(process.stdout.decode())
    else:
        return filename.group(1)


def libreoffice_exec():
    # TODO: Provide support for more platforms
    if sys.platform == 'darwin':
        return '/Applications/LibreOffice.app/Contents/MacOS/soffice'
    return 'libreoffice'


class LibreOfficeError(Exception):
    def __init__(self, output):
        self.output = output


if __name__ == '__main__':
    print('Converted to ' + convert_to())