# -*- coding: utf-8 -*-

from flask_restful import Resource


class App(Resource):

    def get(self):
        return {'app_service_status': 'OK'}