# -*- coding: utf-8 -*-
import os

from core.reapy import ReaPy
from core.model import Model


class Document(object):
    app_path = os.path.abspath(__file__)
    view_path = str(ReaPy.presenter().get_project_root()) + '/apps/views/eng.json'
    messages = ReaPy.presenter().get_message(view_path, 'messages')['user']

    def __init__(self):
        super(Document).__init__()

    def get_doc(self):

        documan = {}

        result = Model('ReaMssql').select('dbo.Documents', ' DocumentOwnerSourceHost, DocumentOwnerSourceType, RequestSchema, DocumentOwnerSourceMethod, DocumentOwnerSourceRequest ', None, None, None).data()

        if result is not None:
            documan['url'] = result[0][0]
            documan['type'] = result[0][1]
            documan['schema'] = result[0][2]
            documan['method'] = result[0][3]
            documan['request_text'] = result[0][4]

        return documan


    # def get_user(self, username, password):
    #     user = {}
    #     user_data = Model().select('public.get_user', None,
    #                                {0: {'col': 'user_name', 'operator': '=', 'value': username}}, None, 1, True).data()
    #
    #     if user_data is None:
    #         return self.messages['errors']['usr_err_100']
    #     db_password = user_data[2]
    #     user_is_active = user_data[7]
    #     user_type_is_active = user_data[8]
    #     if user_is_active is False:
    #         return self.messages['errors']['usr_err_101']
    #     if user_type_is_active is False:
    #         return self.messages['errors']['usr_err_102']
    #     hash_check = ReaPy.hash().verify_password(password, db_password)
    #     if hash_check is False:
    #         return self.messages['errors']['usr_err_103']
    #     user['user_id'] = user_data[0]
    #     user['user_name'] = user_data[1]
    #     user['user_password'] = user_data[2]
    #     user['user_person_name'] = user_data[3]
    #     user['user_person_surname'] = user_data[4]
    #     user['user_email'] = user_data[5]
    #     user['user_type_name'] = user_data[6]
    #     user['user_type_id'] = user_data[10]
    #     user['user_is_active'] = user_data[7]
    #     user['user_token_expire'] = user_data[9]
    #     user_data = self.messages['success']['success_temp']
    #     user_data['data']['payload'] = user
    #     return user_data
    #
    # @staticmethod
    # def get_user_token_type():
    #     token_type = {}
    #     token_data = Model().select('public.get_user_token_type', None,
    #                                 {0: {'col': 'user_token_type_is_active', 'operator': '=', 'value': True}}, None,
    #                                 None,
    #                                 False).data()
    #     if token_data:
    #         i = 0
    #         for token in token_data:
    #             token = {'token_type_id': token[0], 'token_type_name': token[1], 'token_type_key': token[2],
    #                      'token_type_description': token[3]}
    #             token_type[i] = token
    #             i += 1
    #     return token_type
    #
    # def generate_user_token(self, user, token_type_key='bearer'):
    #     try:
    #         set_param = 1
    #         token_type = self.get_user_token_type()
    #         find_token_key = ReaPy.find().in_dict('token_type_key', token_type_key, token_type)
    #         if find_token_key is None:
    #             return self.messages['errors']['usr_err_104']
    #         list_key = list(find_token_key.keys())[0]
    #         token_type_name = find_token_key[list_key]['token_type_description']
    #         jti = ReaPy.hash().hash_password(user['user_id'])
    #         now = datetime.now()
    #         exp = now + timedelta(minutes=user['user_token_expire'])
    #         jwt_token = ReaPy.hash().generate_jwt('ReActor ' + user['user_type_name'], token_type_name, jti, now, exp)
    #         insert_token = Model().insert('user_token', {
    #             0: {'user_token_value': jwt_token, 'user_token_user_id': user['user_id'], 'user_token_expiry_time': exp,
    #                 'user_token_type_id': find_token_key[list_key]['token_type_id']}}).data()
    #         user['user_token'] = jwt_token
    #         Model('ReaRedis').delete(1, {0: {'col': ReaPy.presenter().get_platform() + ':user_auth', 'operator': 'LIKE',
    #                                          'value': user['user_id'] + '*'}}).data()
    #         if user['user_token_expire'] != -1:
    #             set_param = [1, user['user_token_expire']]
    #         insert_cache_token = Model('ReaRedis').insert(set_param if set_param else 1, {
    #             0: {ReaPy.presenter().get_platform() + ':user_auth:' + user['user_id'] + ':' + jwt_token: {
    #                 'user': user}}}).data()
    #         if insert_token and insert_cache_token is None:
    #             return self.messages['errors']['usr_err_105']
    #         token_data = self.messages['success']['success_temp']
    #         return token_data
    #     except Exception as e:
    #         print(e)
    #         return self.messages['errors']['usr_err_105']
    #
    # def get_token(self, token):
    #     user_token_data = Model('ReaRedis').select(1, None,
    #                                                {0: {'col': ReaPy.presenter().get_platform() + ':user_auth',
    #                                                     'operator': 'LIKE',
    #                                                     'value': '*' + token}}).data()
    #     if user_token_data is None:
    #         return self.messages['errors']['usr_err_106']
    #     else:
    #         user = ReaPy.json().loads(user_token_data[0][1])
    #         del user['user']['user_token']
    #         ttl = user_token_data[1]
    #         user_data = self.messages['success']['success_temp']
    #         user_data['data']['payload'] = user['user']
    #         if ttl == -1:
    #             ttl = None
    #         user_data['data']['payload']['user_token_remaining'] = ttl
    #
    #         return user_data
