# -*- coding: utf-8 -*-
import os

from core.reapy import ReaPy
from core.model import Model


class Integrator(object):
    app_path = os.path.abspath(__file__)
    view_path = str(ReaPy.presenter().get_project_root()) + '/apps/tcp_server/views/eng.json'
    messages = ReaPy.presenter().get_message(view_path, 'messages')['integrator']

    def __init__(self):
        super(Integrator).__init__()

    def check_integrator_data(self, client_id, integrator_id, integrator_secret):
        integrator_data = Model().select('integrator', None,
                                         {0: {'col': 'integrator_id', 'operator': '=', 'value': integrator_id}},
                                         None,
                                         1, True).data()
        if integrator_data is None:
            return self.messages['errors']['int_err_100']
        db_secret = integrator_data[3]
        integrator_is_active = integrator_data[4]
        if integrator_is_active is False:
            return self.messages['errors']['int_err_101']
        hash_check = ReaPy.hash().verify_password(integrator_secret, db_secret)
        if hash_check is False:
            return self.messages['errors']['int_err_102']
        token = ReaPy.hash().hash_password(ReaPy.hash().get_uuid().hex)
        insert_db_token = Model().insert('integrator_token', {
            0: {'integrator_token_integrator_id': integrator_id, 'integrator_token_value': token}}).data()
        insert_cache_token = Model('ReaRedis').insert(0, {
            0: {ReaPy.presenter().get_platform() + ':integrator_auth:' + token: {
                'client_id': client_id, 'integrator_id': integrator_id,
                'integrator_token_value': token}}}).data()
        if insert_db_token is None and insert_cache_token is None:
            return self.messages['errors']['int_err_103']
        token_data = self.messages['success']['success_temp']
        token_data['data']['payload']['token'] = token
        return token_data

    @staticmethod
    def delete_client_auth_cache(token):
        delete_cache_token = Model('ReaRedis').delete(0, {
            0: {'col': ReaPy.presenter().get_platform() + ':integrator_auth',
                'operator': '=', 'value': token}}).data()
        return delete_cache_token

    @staticmethod
    def get_client_data(token):
        get_token = Model('ReaRedis').select(0, {
            0: {'col': ReaPy.presenter().get_platform() + ':integrator_auth',
                'operator': '=', 'value': token}}).data()
        return get_token

    @staticmethod
    def delete_auth_cache():
        delete_cache_token = Model('ReaRedis').delete(0, {
            0: {'col': ReaPy.presenter().get_platform(),
                'operator': 'LIKE', 'value': 'integrator_auth*'}}).data()
        return delete_cache_token
