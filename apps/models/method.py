# -*- coding: utf-8 -*-
from core.reapy import ReaPy
from apps.tcp_server.models.user import User


class Method(object):

    def __init__(self, master):
        super(Method).__init__()
        self.master = master
        self.master_client = self.master.client

    def method_payload_validate(self, schema, data):
        validate = ReaPy.validator().json_validate(self.master.schema[schema], data)
        if validate['success'] is False:
            message = self.master.messages['errors']['errors_temp']
            message['data']['description'] = validate['message']
            self.master_client.send_data(message)
            return False

    def user_login(self, data):
        self.method_payload_validate('method_user_login', data['data'])
        user_data = User().get_user(data['data']['username'], data['data']['password'])
        if user_data['data']['success'] is True:
            try:
                User().generate_user_token(user_data['data']['payload'], data['data']['token_type'])
            except Exception as e:
                print(e)
        self.master_client.send_data(user_data)

    def check_user_token(self, data):
        self.method_payload_validate('method_check_user_token', data['data'])
        user_token_data = User().get_token(data['data']['user_token'])
        self.master_client.send_data(user_token_data)
